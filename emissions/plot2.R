#Assume all files are in the working directory
NEI <- readRDS("datasets/summarySCC_PM25.rds")
#Filter data of Baltimore City, Maryland
NEIBT<-NEI[NEI$fips=="24510",]
#Sum all emissions by year
emyearBtmr<-tapply(NEIBT$Emissions, NEIBT$year, FUN=sum)
#Start the device to export to a png
png('plot2.png')
#Plot a line
plot(names(emyearBtmr),emyearBtmr,pch=2, type="l" ,main = "Baltimore City Annual Emissions of Particulate Matter 2.5 ", ylab = "Tons", xlab = "")
dev.off()
