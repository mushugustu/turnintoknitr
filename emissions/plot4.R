#Assume all files are in the working directory
NEI <- readRDS("datasets/summarySCC_PM25.rds")
SCC <- readRDS("datasets/Source_Classification_Code.rds")
load("data.table")
SCC<-data.table(SCC)
#Filter by Coal Emissions
coalFilter<-toupper(SCC$EI.Sector) %like% "COAL"
NEICOAL<-NEI[coalFilter,]
emyearCoal<-tapply(NEICOAL$Emissions, NEICOAL$year, FUN=sum)
#Start the device to export to a png
png('plot4.png')
barplot(emyearCoal, main = "Anual Coal Emissions of PM 2.5 across U.S. ", ylab = "Tons")
dev.off()